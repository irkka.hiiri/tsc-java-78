FROM maven:3-jdk-8

COPY ./ /opt/taskmanager
WORKDIR /opt/taskmanager

RUN mvn clean package -DskipTests

EXPOSE 8080
ENTRYPOINT ["java", "-jar", "./target/taskmanager.war"]
