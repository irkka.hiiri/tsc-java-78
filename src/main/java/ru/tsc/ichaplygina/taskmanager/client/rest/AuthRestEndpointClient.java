package ru.tsc.ichaplygina.taskmanager.client.rest;

import feign.Feign;
import feign.okhttp.OkHttpClient;
import okhttp3.JavaNetCookieJar;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.tsc.ichaplygina.taskmanager.model.Result;

import java.net.CookieManager;
import java.net.CookiePolicy;

public interface AuthRestEndpointClient {

    String BASE_URL = "http://localhost:8080/auth/";

    static AuthRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;

        final CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);

        final okhttp3.OkHttpClient.Builder builder = new okhttp3.OkHttpClient().newBuilder();
        builder.cookieJar(new JavaNetCookieJar(cookieManager));

        return Feign.builder()
                .client(new OkHttpClient(builder.build()))
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(AuthRestEndpointClient.class, BASE_URL);
    }

    @GetMapping(value = "/login",
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = "Accept=application/json")
    Result login(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password
    );

    @GetMapping(value = "/session",
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = "Accept=application/json")
    Authentication session();

    @GetMapping(value = "/profile",
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = "Accept=application/json")
    User user();

    @GetMapping(value = "/logout",
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = "Accept=application/json")
    Result logout();


}
