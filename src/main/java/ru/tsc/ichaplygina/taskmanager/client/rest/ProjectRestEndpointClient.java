package ru.tsc.ichaplygina.taskmanager.client.rest;

import feign.Feign;
import feign.auth.BasicAuthRequestInterceptor;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.net.CookieManager;
import java.net.CookiePolicy;

public interface ProjectRestEndpointClient {

    String BASE_URL = "http://localhost:8080/api/project/";

    static ProjectRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        final CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        final OkHttpClient.Builder builder = new okhttp3.OkHttpClient().newBuilder();
        builder.cookieJar(new JavaNetCookieJar(cookieManager));
        return Feign.builder()
                .client(new feign.okhttp.OkHttpClient(builder.build()))
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .requestInterceptor(new BasicAuthRequestInterceptor("user", "user"))
                .target(ProjectRestEndpointClient.class, BASE_URL);
    }

    @PostMapping(value = "add",
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = "Accept=application/json")
    Project add(@RequestBody Project project);

    @GetMapping(value = "findById/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = "Accept=application/json")
    Project findById(@PathVariable("id") String id);

    @DeleteMapping(value = "removeById/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = "Accept=application/json")
    void removeById(@PathVariable("id") String id);

    @PutMapping(value = "save",
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = "Accept=application/json")
    void save(@RequestBody Project project);

}
