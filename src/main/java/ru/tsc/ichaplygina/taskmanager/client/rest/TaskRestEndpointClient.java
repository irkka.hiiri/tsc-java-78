package ru.tsc.ichaplygina.taskmanager.client.rest;

import feign.Feign;
import okhttp3.JavaNetCookieJar;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.net.CookieManager;
import java.net.CookiePolicy;

public interface TaskRestEndpointClient {

    String BASE_URL = "http://localhost:8080/api/task/";

    static TaskRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;

        final CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);

        final okhttp3.OkHttpClient.Builder builder = new okhttp3.OkHttpClient().newBuilder();
        builder.cookieJar(new JavaNetCookieJar(cookieManager));

        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskRestEndpointClient.class, BASE_URL);
    }

    @PostMapping("add")
    Task add(@RequestBody Task task);

    @GetMapping("findById/{id}")
    Task findById(@PathVariable("id") String id);

    @DeleteMapping("removeById/{id}")
    void removeById(@PathVariable("id") String id);

    @PutMapping("save")
    void save(@RequestBody Task task);

}
