package ru.tsc.ichaplygina.taskmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.service.ProjectService;
import ru.tsc.ichaplygina.taskmanager.util.UserUtil;

@Controller
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping("/project/create")
    public String create() {
        projectService.write(UserUtil.getUserId(), new Project("New Project " + System.currentTimeMillis()));
        return "redirect:/projects";
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping("/project/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        projectService.removeById(UserUtil.getUserId(), id);
        return "redirect:/projects";
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @PostMapping("/project/edit/{id}")
    public String edit(@ModelAttribute("project") Project project, BindingResult result) {
        projectService.write(UserUtil.getUserId(), project);
        return "redirect:/projects";
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        final Project project = projectService.findById(UserUtil.getUserId(), id);
        return new ModelAndView("project-edit", "project", project);
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }
}
