package ru.tsc.ichaplygina.taskmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.ichaplygina.taskmanager.service.TaskService;
import ru.tsc.ichaplygina.taskmanager.util.UserUtil;

@Controller
public class TasksController {

    @Autowired
    private TaskService taskService;

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping("/tasks")
    public ModelAndView index() {
        return new ModelAndView("task-list", "tasks", taskService.findAll(UserUtil.getUserId()));
    }

}
