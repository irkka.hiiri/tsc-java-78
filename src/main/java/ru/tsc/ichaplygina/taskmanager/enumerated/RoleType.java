package ru.tsc.ichaplygina.taskmanager.enumerated;

public enum RoleType {

    USER,
    ADMIN;

}
