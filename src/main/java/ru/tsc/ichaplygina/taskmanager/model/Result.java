package ru.tsc.ichaplygina.taskmanager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@NoArgsConstructor
public class Result {

    public boolean success = true;

    @Nullable
    public String message;

    public Result(final boolean success) {
        this.success = success;
    }

    public Result(@NotNull final Exception e) {
        success = false;
        message = e.getMessage();
    }

}
