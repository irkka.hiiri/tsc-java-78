package ru.tsc.ichaplygina.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.repository.ProjectRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProjectService {

    @NotNull
    @Autowired
    private ProjectRepository repository;


    public List<Project> findAll(@NotNull final String userId) {
        return repository.findAllByUserId(userId);
    }

    public Project findById(@NotNull final String userId, @NotNull final String id) {
        return repository.findByUserIdAndId(userId, id);
    }

    @Transactional
    public void remove(@NotNull final String userId, List<Project> projects) {
        for (Project project : projects) project.setUserId(userId);
        repository.deleteAll(projects
                .stream()
                .filter(t -> userId.equals(t.getUserId()))
                .collect(Collectors.toList())
        );
    }

    @Transactional
    public void removeAll(@NotNull final String userId) {
        repository.deleteAllByUserId(userId);
    }

    @Transactional
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        repository.deleteByUserIdAndId(userId, id);
    }

    @Nullable
    @Transactional
    public Project write(@NotNull final String userId, @NotNull final Project project) {
        project.setUserId(userId);
        return repository.save(project);
    }

    @Transactional
    @NotNull
    public List<Project> write(@NotNull final String userId, @NotNull final List<Project> projects) {
        for (Project project : projects) project.setUserId(userId);
        return repository.saveAll(projects);
    }
}
